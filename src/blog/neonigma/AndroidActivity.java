package blog.neonigma;

import src.blog.neonigma.R;
import android.app.Activity;
import android.os.Bundle;
import android.widget.Toast;

public class AndroidActivity extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        String greeting = SayHello("neonigma");
        Toast.makeText(this, greeting, Toast.LENGTH_LONG).show();
    }
    
    public native String SayHello(String name);
    
    static {
        System.loadLibrary("mixed_sample");
    }
}